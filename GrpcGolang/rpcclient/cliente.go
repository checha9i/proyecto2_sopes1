package rpcclient

import (
	"log"

	Phyton "../mcache"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func Envio(nombre string, dep string, edad int32, conta string, estado string) string {
	var conn *grpc.ClientConn
	conn, err := grpc.Dial("sopesmaquina2.top.mx:30002", grpc.WithInsecure())
	//conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := Phyton.NewCacheClient(conn)
	response, err := c.Add(context.Background(), &Phyton.Entry{Nombre: nombre, Departamento: dep, Edad: edad, Contagio: conta, Estado: estado})
	if err != nil {
		log.Fatalf("Error en la llamada Add: %s", err)
		return "error"
	}
	return response.Response
}
