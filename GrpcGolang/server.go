package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	cliente "./rpcclient"
	"github.com/rs/cors"
)

type recibe struct {
	Nombre       string `json:"Nombre"`
	Departamento string `json:"Departamento"`
	Edad         int32  `json:"Edad"`
	Contagio     string `json:"Contagio"`
	Estado       string `json:"Estado"`
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/put", func(w http.ResponseWriter, r *http.Request) {
		var p recibe
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
        	w.Header().Set("Content-Type", "application/json")
		s, err := ioutil.ReadAll(r.Body)
		if err != nil {
			panic(err) // This would normally be a normal Error http response but I've put this here so it's easy for you to test.
		}
		err = json.Unmarshal(s, &p)
		if err != nil {
			panic(err) // This would normally be a normal Error http response but I've put this here so it's easy for you to test.
		}
		fmt.Println(p.Nombre)
		var ret = cliente.Envio(p.Nombre, p.Departamento, p.Edad, p.Contagio, p.Estado)
		w.Write([]byte("{\"respuesta\": \"" + ret + "\"}"))
	})
	// cors.Default() setup the middleware with default options being
	// all origins accepted with simple methods (GET, POST).
	handler := cors.Default().Handler(mux)
	http.ListenAndServe(":9090", handler)
}
