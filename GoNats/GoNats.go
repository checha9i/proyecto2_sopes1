package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	nats "github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"net/http"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"io/ioutil"
)

var (
	clusterID string
	clientID  string
	URL       string
	async     bool
)

func usage() {
	usageStr := `Usage: stan-pub`
	fmt.Printf("%s\n", usageStr)
	os.Exit(0)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func info(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil{
		fmt.Println(err)
	}
	/**
	* NATS
	*/
	
	opts := []nats.Option{nats.Name("Nats Publisher")}

	nc, err := nats.Connect(URL, opts...)
	failOnError(err, "Error en nats.Connect")

	defer nc.Close()

	sc, err := stan.Connect("test-cluster", "device-3F45", stan.NatsConn(nc))
	failOnError(err, "Error en stan.Connect")

	defer sc.Close()

	if !async {
			subj, msg := "lab", []byte(body)
			err = sc.Publish(subj, msg)
			failOnError(err, "Error en la publicación")
			log.Printf("Publicado [%s]: %s \n", subj, msg)
	}
	/**
	*NATS
	*/
	
	fmt.Fprintf(w, "%s", "termino nati na")
}

func main() {
	/*NATS*/
	flag.StringVar(&URL, "s", "nats://167.172.125.89:4222", "")
	flag.StringVar(&URL, "server", "nats://167.172.125.89:4222", "")
	flag.StringVar(&clusterID, "c", "test-cluster", "")
	flag.StringVar(&clusterID, "cluster", "test-cluster", "")
	flag.StringVar(&clientID, "id", "stan-pub", "")
	flag.StringVar(&clientID, "clientid", "stan-pub", "")
	flag.BoolVar(&async, "a", false, "")
	flag.BoolVar(&async, "async", false, "")

	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()
	/*NATS*/
	router := mux.NewRouter().StrictSlash(true)
	fmt.Println("Server Running on port: 8081")
	router.HandleFunc("/", info).Methods("POST")

	// cors.Default() setup the middleware with default options being
    // all origins accepted with simple methods (GET, POST). See
    // documentation below for more options.
    handler := cors.Default().Handler(router)
    http.ListenAndServe(":8081", handler)
	
	log.Fatal(http.ListenAndServe(":8081", router))
}