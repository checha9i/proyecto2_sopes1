export PATH=$PATH:/usr/local/go/bin

docker login 
docker build -t gonats .
docker tag gonats checha9i/gonats
docker push checha9i/gonats

//crear el deployment
kubectl create deployment gonats -n proyecto --image=checha9i/gonats
kubectl -n proyecto expose deployment gonats --port 8081 --target-port=8081 --type NodePort --name=gonats-service
kubectl -n proyecto apply -f gonats-ingress.yml