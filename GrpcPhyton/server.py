from concurrent import futures
import grpc

import mcache_pb2_grpc
from echoer import Echoer


class Server:

    @staticmethod
    def run():
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        mcache_pb2_grpc.add_CacheServicer_to_server(Echoer(), server)
        server.add_insecure_port('[::]:50051')
        server.start()
        server.wait_for_termination()