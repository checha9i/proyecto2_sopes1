# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import mcache_pb2 as mcache__pb2


class CacheStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Add = channel.unary_unary(
                '/mcache.Cache/Add',
                request_serializer=mcache__pb2.Entry.SerializeToString,
                response_deserializer=mcache__pb2.REntry.FromString,
                )


class CacheServicer(object):
    """Missing associated documentation comment in .proto file."""

    def Add(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_CacheServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'Add': grpc.unary_unary_rpc_method_handler(
                    servicer.Add,
                    request_deserializer=mcache__pb2.Entry.FromString,
                    response_serializer=mcache__pb2.REntry.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'mcache.Cache', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class Cache(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def Add(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/mcache.Cache/Add',
            mcache__pb2.Entry.SerializeToString,
            mcache__pb2.REntry.FromString,
            options, channel_credentials,
            call_credentials, compression, wait_for_ready, timeout, metadata)
