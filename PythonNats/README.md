pip install nats-python
pip install asyncio-nats-client

docker login 
docker build -t pythonnats .
docker tag pythonnats checha9i/pythonnats
docker push checha9i/pythonnats

kubectl create deployment pythonnats -n proyecto --image=checha9i/pythonnats

// Para borrar el deployment
//  kubectl delete -n proyecto deployment pythonnats
