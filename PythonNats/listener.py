import asyncio
import signal
import sys
from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN
import redis
import json
import pymongo
import flask
import requests

import opentelemetry.ext.requests
from opentelemetry import trace
from opentelemetry.ext import jaeger
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import ConsoleSpanExporter
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor
from opentelemetry.ext.flask import FlaskInstrumentor

# jaeger
jaeger_exporter = jaeger.JaegerSpanExporter(
    service_name="my-traced-service", agent_host_name="simplest-agent.proyecto.svc.cluster.local", agent_port=6831
)

trace.set_tracer_provider(TracerProvider())
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(jaeger_exporter)
)

app = flask.Flask(__name__)

def convert(data):
	if isinstance(data, bytes):  return data.decode('ascii')
	if isinstance(data, dict):   return dict(map(convert, data.items()))
	if isinstance(data, tuple):  return map(convert, data)
	return data

#MONGODB
db = "CORONAVIRUS"
collection = "PACIENTES" #Mismo valor para mongo y redis
#DEFINICION DE LLAVES PARA EL JSON
NOMBRE = "nombre"
DEPARTAMENTO = "departamento"
EDAD = "edad"
FORMA = "forma de contagio"
ESTADO = "estado"

#REDIS
CONTADOR = "CONTADOR"
IPREDIS = "157.245.186.202"

myclient = pymongo.MongoClient(host=IPREDIS, port=27017)
mydb = myclient[convert(db)]
mycol = mydb[convert(collection)]
collist = mydb.list_collection_names()
if convert(collection) in collist:
	print("The collection exists.")	

async def run(loop):
    nc = NATS()
    await nc.connect(io_loop=loop, servers=["nats://167.172.125.89:4222"])

    sc = STAN()
    await sc.connect("test-cluster","listener-3F45",nats=nc)

    async def cb(msg):

        
                    
        print("Mensaje: (#{}): {}".format(msg.seq,msg.data))
        #mongodb
        conversion = convert(msg.data)
        print("CONVERSION " + conversion)
        print("VARIABLE Y ")
        y = json.loads(conversion)
        print(y[NOMBRE])
        x = mycol.insert_one(y)
            
        #redis
        r = redis.StrictRedis(host=IPREDIS, port=6379,db=0)
        pivote = convert(r.get(CONTADOR))

        r.hset(collection,NOMBRE+"["+pivote+"]", y[NOMBRE])
        r.hset(collection,DEPARTAMENTO+"["+pivote+"]", y[DEPARTAMENTO])
        r.hset(collection,EDAD+"["+pivote+"]", y[EDAD])
        r.hset(collection,FORMA+"["+pivote+"]", y[FORMA])
        r.hset(collection,ESTADO+"["+pivote+"]", y[ESTADO])
        pivateInt = int(pivote) + 1
        r.set(CONTADOR,pivateInt)
            
        tracer = trace.get_tracer(__name__)
        with tracer.start_as_current_span("insercion mongo"):
                with tracer.start_as_current_span("insercion redis"):
                    requests.get("https://www.google.com")

    subject = "lab"
    await sc.subscribe(subject, durable_name="3f45",cb=cb)

if __name__ == '__main__':
    print('hola mundo')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.run_forever()

