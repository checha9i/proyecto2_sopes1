package main
import (
	"fmt"
	"strings"
	"bufio"
	"strconv"
	"encoding/json"
	"net/http"
	"io/ioutil"
	"bytes"
	"os"
	"log"
)

//Se almacenará un arreglo de casos de COVID-19
type Casos struct {
    Casos []caso `json:"Casos"`
}

//Struct para almacenar los casos de COVID-19
type caso struct {
    Nombre   string `json:"nombre"`
    Departamento   string `json:"departamento"`
    Edad    int    `json:"edad"`
	Forma string `json:"forma de contagio"`
	Estado   string `json:"estado"`
}

func hiloEjec(canales chan string, index int, numDatos int, url string, ruta string, info_env int) {
	archivoJson, err := os.Open(ruta)
	if err != nil {
		fmt.Println(err)
	}
	defer archivoJson.Close()
	byteValue, _ := ioutil.ReadAll(archivoJson)
	var listaCasos Casos
	json.Unmarshal(byteValue, &listaCasos)
	for i := index * info_env; i < index*info_env + info_env && i < numDatos; i++ {
		fmt.Println("index: ", index, " info_env: ", info_env, " i ", i)
		casoActual, _ := json.Marshal(listaCasos.Casos[i])
		respon, err := http.Post("http://"+url, "application/json", bytes.NewBuffer(casoActual))
		if err != nil {
			log.Println(err)
		}	
		defer respon.Body.Close()
		body, err := ioutil.ReadAll(respon.Body)
		if err != nil {
			log.Println(err)
		}
		fmt.Println(string(body))
	}	
	cadena := fmt.Sprintf("%s%d","Hilo: ", index)
    canales <- cadena
}

func main() {
	campo := [4]string{"Ingrese el Load Balancer a seleccionar:","Numero de hilos a enviar:","Numero de solicitudes del archivo (cantidad de casos a enviar):","Ingrese la ruta del archivo:"}
	var valores [4]string
	var ctl=0
	for {
		if ctl == 4 {
			break
		}
		fmt.Println(campo[ctl])
		reader := bufio.NewReader(os.Stdin)
		entrada, _ := reader.ReadString('\n')
		valores[ctl] = strings.TrimRight(entrada, "\r\n")
		ctl++;
	}	
	numHilos, err := strconv.Atoi(valores[1])
	if err != nil {
		fmt.Println("No se agrego un numero de hilos valido, 1 hilo por defecto...")
		numHilos = 1
	}
	numDatos, err := strconv.Atoi(valores[2])
	if err != nil {
		fmt.Println("No se agrego un numero de datos valido, 1 dato por defecto...")
		numDatos = 1
	}
	canales := make(chan string, numHilos)
	for i := 0; i < numHilos; i++ {
		info := numDatos/numHilos + 1
		go hiloEjec(canales,i, numDatos, valores[0], valores[3], info)
    }
	contador := 0 
    for element := range canales {
        if contador == numHilos - 1 {
            close(canales)        
        }
        contador++
        fmt.Println(element)
    }
}