docker login
docker build -t webpage .
docker tag webpage checha9i/webpage
docker push checha9i/webpage

kubectl create deployment webpage -n proyecto --image=checha9i/webpage
kubectl -n proyecto expose deployment webpage --port 3000 --target-port=3000 --type LoadBalancer --name=webpage-service
