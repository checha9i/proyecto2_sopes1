const { io } = require('./ServerExpress');
const { MongoClient } = require('./ServerExpress');
const { redis } = require('./ServerExpress');

//constantes
const NOMBRE = "nombre"
const DEPARTAMENTO = "departamento"
const EDAD = "edad"
const FORMA = "forma de contagio"
const ESTADO = "estado"

//conexion
io.on('connection', async(client) => {
    try {
        console.log('Conexion Establecida');
        let redis = await getRedis();
        let mongo = await getMongo();
        client.emit('data', {
            redis: redis,
            mongo: mongo
        });

        client.on('disconnect', () => {
            console.log('Conexion Terminada');
        });

        //Escuchar el cliente
        client.on(' sendData', async(data, callback) => {
            //console.log(data);
            let redis = await getRedis();
            let mongo = await getMongo();
            client.emit(' sendData', {
                redis: redis,
                mongo: mongo
            });
        });
    } catch (error) {
        client.emit('data', {
            error: error.toString()
        });
    }
});

async function getMongo() {
    let client;
    try {
        client = await MongoClient.connect('mongodb://157.245.186.202:27017', { useNewUrlParser: true, useUnifiedTopology: true });
        const db = client.db('CORONAVIRUS');
        const personCollection = db.collection('PACIENTES');

        let piegraph = await personCollection.aggregate([
            { $match: {} }, {
                $group: { _id: '$departamento', total: { $sum: 1 } }
            }
        ]).sort({ total: -1 }).toArray();

        let top3 = await personCollection.aggregate([
            { $match: {} }, {
                $group: { _id: '$departamento', total: { $sum: 1 } }
            }
        ]).sort({ total: -1 }).limit(3).toArray();

        let alldata = await personCollection.find({}).sort({ nombre: 1 }).toArray();
        return {
            piegraph: piegraph,
            top3: top3,
            alldata: alldata
        };
    } catch (e) {
        console.error(e);
    } finally {
        client.close();
    }
}

async function getRedis() {
    let vector = [];
    let name = "",
        age = "",
        depto = "",
        form = "",
        status = "";
    return new Promise(function(resolve, reject) {
        const clt = redis.createClient({
            host: '157.245.186.202',
            port: 6379
        });
        clt.on("error", function(error) {
            console.error(error);
        });
        clt.get("CONTADOR", function(err, req) {
            if (err) {
                throw new Error(err);
            }
            if (req != null) {
                let valor = req.toString();
                valor = valor - 1;
                let key = "nombre[" + valor.toString() + "]";
                clt.hget("PACIENTES", key, function(err, result) {
                    if (err) throw err;
                    name = result;

                });

                key = "Estado[".toLowerCase() + valor.toString() + "]";
                clt.hget("PACIENTES", key, function(err, result) {
                    if (err) throw err;
                    status = result;
                });

                key = "departamento[" + valor.toString() + "]";
                clt.hget("PACIENTES", key, function(err, result) {
                    if (err) throw err;
                    depto = result;
                });

                key = "forma de Contagio[".toLowerCase() + valor.toString() + "]";
                clt.hget("PACIENTES", key, function(err, result) {
                    if (err) throw err;
                    form = result;
                });

                key = "edad[" + valor.toString() + "]";
                clt.hget("PACIENTES", key, function(err, result) {
                    if (err) throw err;
                    age = result;
                });
            }
        });


        clt.hgetall("PACIENTES", function(err, value) {
            clt.get("CONTADOR", function(err, req) {
                if (err) {
                    throw new Error(err);
                }

                let valor = req.toString();
                valor = valor - 1;
                for (let x = 0; x <= valor; x++) {
                    let edad = Number(value[EDAD + "[" + x.toString() + "]"]);
                    edad = edad / 10;
                    let intvalue = Math.floor(edad);
                    let contador = 1;
                    if (vector[intvalue] !== undefined) {
                        contador = vector[intvalue] + 1;
                    }
                    vector[intvalue] = contador;
                }
                let ultimoinfectado = {
                    NOMBRE: name,
                    EDAD: age,
                    ESTADO: status,
                    FORMA: form,
                    DEPARTAMENTO: depto
                };
                let redisQuery = {
                    last_case: ultimoinfectado,
                    barras: vector
                }
                resolve(redisQuery);

            });
        });
    });
}